<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="description" content="Fluent Bit workshop">
		<meta name="author" content="Eric D. Schabell">

		<title>Telemetry Pipelines Workshop</title>

		<link rel="stylesheet" href="dist/reset.css">
		<link rel="stylesheet" href="dist/reveal.css">
		<link rel="stylesheet" href="dist/theme/chrono.css">

		<!-- Theme used for syntax highlighted code -->
		<link rel="stylesheet" href="plugin/highlight/monokai.css">
	</head>

	<!-- Google tag (gtag.js) -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-799E08H5WD"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'G-799E08H5WD');
	</script>

	<body>
		<div class="reveal">
			<div class="slides">
				<section>
					<h3 class="r-fit-text">Lab 7 - Pipeline integration with OpenTelemetry</h3>
				</section>
				<section>
					<div style="height: 250px;">
						<h2>Lab Goal</h2>
						<h4>
							This lab explores how to integrate a Fluent Bit pipeline with OpenTelemetry.
						</h4>
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">Intermezzo - Jumping to the solution</h3>
					</div>
					<div style="height: 150px; text-align: left; font-size: x-large;">
						If you happen to be exploring Fluent Bit as an architect and want to jump to the solution in
						action, we've included the configuration files in the easy install project from the source
						install support directory, see the previous installing from source lab. Instead of creating all
						the configurations as shown in this lab, you'll find them ready to use as shown below from the
						<code><b>fluentbit-install-demo</b></code> root directory:
					</div>
					<div style="height: 100px; text-align: left;">
						<pre>
							<code data-trim data-noescape>
								$ ls -l support/configs-lab-7/

								-rw-r--r--@ 1 erics  staff   166 Aug  1 15:34 Buildfile-fb
								-rw-r--r--  1 erics  staff   215 Jul 27 09:47 Buildfile-otel
								-rw-r--r--  1 erics  staff    73 Jul 25 13:51 Buildfile-prom
								-rw-r--r--  1 erics  staff  1589 Aug  1 15:50 workshop-fb.yaml
								-rw-r--r--  1 erics  staff   245 Jul 30 15:42 workshop-otel.yaml
								-rw-r--r--  1 erics  staff   209 Jul 27 09:23 workshop-prom.yml
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">OTel integration - The use cases</h3>
					</div>
					<div style="height: 170px; text-align: left; font-size: x-large;">
						There are many reasons why an organization might want to pass their telemetry pipeline output
						onwards to OpenTelemetry (OTel), specifically through an
						<a href="https://opentelemetry.io/docs/collector/" target="_blank">OpenTelemetry Collector</a>.
						To facilitate this integration, Fluent Bit from the release of version 3.1 added support for
						converting its telemetry data into the OTel format.<br />
						<br />
						In this lab, we'll explore what Fluent Bit offers to convert pipeline data into the correct
						format for sending onwards to OTel.
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">OTel integration - The architecture</h3>
					</div>
					<div style="height: 110px; text-align: left; font-size: x-large;">
						This is an architectural overview of what we are building. Here you can see the pipeline
						collecting log events, processing them into an OTel Envelope to satisfy the OTel schema
						(resource, scope, attributes), pushing to an OTel Collector, and finally to both the collectors
						console output and to a separate log file:
					</div>
					<div style="height: 420px;">
						<img src="images/lab07-1.png" alt="architecture">
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">OTel integration - Initial log configuration</h3>
					</div>
					<div style="height: 50px; text-align: left; font-size: x-large;">
						We begin configuration of our telemetry pipeline in the <code><b>INPUT</b></code> phase with a
						simple dummy plugin generating sample log events as follows:
					</div>
					<div style="height: 150px; text-align: left; font-size: xx-large;">
						<pre>
							<code data-trim data-noescape>
								# This file is our workshop Fluent Bit configuration.
								#
								service:
								  flush: 1
								  log_level: info

								pipeline:

								  # This entry generates a success message for the workshop.
								  inputs:
									- name: dummy
								      dummy: '{"service" : "backend", "log_entry" : "Generating a 200 success code."}'
								...
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">OTel integration - Initial configuration outputs</h3>
					</div>
					<div style="height: 70px; text-align: left; font-size: xx-large;">
						Now ensure the output section of our configuration file <code><b>workshop-fb.yaml</b></code>
						following the inputs section is as follows:
					</div>
					<div style="height: 200px; text-align: left; font-size: xxx-large;">
						<pre>
							<code data-trim data-noescape>
								# This entry directs all tags (it matches any we encounter)
								# to print to standard output, which is our console.
								#
								outputs:
								  - name: stdout
								    match: '*'
								    format: json_lines
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">OTel integration - Running initial pipeline (source)</h3>
					</div>
					<div style="height: 220px; text-align: left; font-size: xx-large;">
						To see if our configuration works we can test run it with our Fluent Bit installation. Depending
						on the chosen install method, here we show how to run it using the source installation followed
						by the container version. Below the source install is shown from the directory we created to hold
						all our configuration files:
					</div>
					<div style="height: 100px; text-align: left; font-size: xxx-large;">
						<pre>
							<code data-trim data-noescape>
								# source install.
								#
								$ [PATH_TO]/fluent-bit --config=workshop-fb.yaml
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">OTel integration - Console output initial pipeline (source)</h3>
					</div>
					<div style="height: 80px; text-align: left; font-size: xx-large;">
						The console output should look something like this. This runs until exiting with CTRL_C:
					</div>
					<div style="height: 100px; text-align: left; font-size: xx-large;">
						<pre>
							<code data-trim data-noescape>
								...
								[2024/08/01 15:41:55] [ info] [input:dummy:dummy.0] initializing
								[2024/08/01 15:41:55] [ info] [input:dummy:dummy.0] storage_strategy='memory' (memory only)
								[2024/08/01 15:41:55] [ info] [output:stdout:stdout.0] worker #0 started
								[2024/08/01 15:41:55] [ info] [sp] stream processor started
								{"date":1722519715.919221,"service":"backend","log_entry":"Generating a 200 success code."}
								{"date":1722519716.252186,"service":"backend","log_entry":"Generating a 200 success code."}
								{"date":1722519716.583481,"service":"backend","log_entry":"Generating a 200 success code."}
								{"date":1722519716.917044,"service":"backend","log_entry":"Generating a 200 success code."}
								{"date":1722519717.250669,"service":"backend","log_entry":"Generating a 200 success code."}
								{"date":1722519717.584412,"service":"backend","log_entry":"Generating a 200 success code."}
								...
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">OTel integration - Testing initial pipeline (container)</h3>
					</div>
					<div style="height: 250px; text-align: left; font-size: xx-large;">
						Let's now try testing our configuration by running it using a container image. First thing that
						is needed is to open in our favorite editor, a new file called
						<code><b>Buildfile-fb</b></code>. This is going to be used to build a new container image
						and insert our configuration. Note this file needs to be in the same directory as our
						configuration file, otherwise adjust the file path names:
					</div>
					<div style="height: 150px; text-align: left; font-size: xxx-large;">
						<pre>
							<code data-trim data-noescape>
								FROM cr.fluentbit.io/fluent/fluent-bit:3.2.2

								COPY ./workshop-fb.yaml /fluent-bit/etc/workshop-fb.yaml

								CMD [ "fluent-bit", "-c", "/fluent-bit/etc/workshop-fb.yaml"]
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">OTel integration - Building initial pipeline (container)</h3>
					</div>
					<div style="height: 120px; text-align: left; font-size: xx-large;">
						Now we'll build a new container image, naming it with a version tag, as follows using the
						<code><b>Buildfile-fb</b></code> and assuming you are in the same directory:
					</div>
					<div style="height: 200px; text-align: left; font-size: xx-large;">
						<pre>
							<code data-trim data-noescape>
								$ podman build -t workshop-fb:v12 -f Buildfile-fb

								STEP 1/3: FROM cr.fluentbit.io/fluent/fluent-bit:3.2.2
								STEP 2/3: COPY ./workshop-fb.yaml /fluent-bit/etc/workshop-fb.yaml
								--> b4ed3356a842
								STEP 3/3: CMD [ "fluent-bit", "-c", "/fluent-bit/etc/workshop-fb.yaml"]
								COMMIT workshop-fb:v4
								--> bcd69f8a85a0
								Successfully tagged localhost/workshop-fb:v12
								bcd69f8a85a024ac39604013bdf847131ddb06b1827aae91812b57479009e79a
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">OTel integration - Running initial pipeline (container)</h3>
					</div>
					<div style="height: 50px; text-align: left; font-size: xx-large;">
						Run the initial pipeline using this container command:
					</div>
					<div style="height: 150px; text-align: left; font-size: xxx-large;">
						<pre>
							<code data-trim data-noescape>
								$ podman run --rm --name fbv12 workshop-fb:v12
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">OTel integration - Console output initial pipeline (container)</h3>
					</div>
					<div style="height: 80px; text-align: left; font-size: xx-large;">
						The console output should look something like this. This runs until exiting with CTRL_C:
					</div>
					<div style="height: 100px; text-align: left; font-size: x-large;">
						<pre>
							<code data-trim data-noescape>
								...
								[2024/08/01 15:41:55] [ info] [input:dummy:dummy.0] initializing
								[2024/08/01 15:41:55] [ info] [input:dummy:dummy.0] storage_strategy='memory' (memory only)
								[2024/08/01 15:41:55] [ info] [output:stdout:stdout.0] worker #0 started
								[2024/08/01 15:41:55] [ info] [sp] stream processor started
								{"date":1722519715.919221,"service":"backend","log_entry":"Generating a 200 success code."}
								{"date":1722519716.252186,"service":"backend","log_entry":"Generating a 200 success code."}
								{"date":1722519716.583481,"service":"backend","log_entry":"Generating a 200 success code."}
								{"date":1722519716.917044,"service":"backend","log_entry":"Generating a 200 success code."}
								{"date":1722519717.250669,"service":"backend","log_entry":"Generating a 200 success code."}
								{"date":1722519717.584412,"service":"backend","log_entry":"Generating a 200 success code."}
								...
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">OTel integration - OpenTelemetry envelope</h3>
					</div>
					<div style="height: 170px; text-align: left; font-size: xx-large;">
						Within the pipeline after ingesting log events, we need to process them into an OTel compatible
						schema before we can push them onwards to an OTel Collector. Fluent Bit v3.1+ offers a processor
						to put our logs into an OTel envelope.<br />
						<br />
						The OTel Envelope is putting our log events into the correct format (<code><b>resource</b></code>,
						<code><b>scope</b></code>, <code><b>attributes</b></code>). Let's try adding this to our
						<code><b>workshop-fb.yaml</b></code> file and explore the changes to our log events...
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">OTel integration - Processing the envelope </h3>
					</div>
					<div style="height: 170px; text-align: left; font-size: xx-large;">
						The configuration of our <code><b>processor</b></code> section needs an entry for logs. Fluent
						Bit provides a simple processor called <code><b>opentelemetry_envelope</b></code> and is added
						as follows after the inputs section:
					</div>
					<div style="height: 200px; text-align: left; font-size: xx-large;">
						<pre>
							<code data-trim data-noescape>
								...
								pipeline:

								  # This entry generates an success message for the workshop.
								  inputs:
								    - name: dummy
								      dummy: '{"service" : "backend", "log_entry" : "Generating a 200 success code."}'

								      processors:
								        logs:
								          - name: opentelemetry_envelope
								...
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">OTel integration - Testing envelope pipeline (source)</h3>
					</div>
					<div style="height: 220px; text-align: left; font-size: xx-large;">
						To see if our configuration works we can test run it with our Fluent Bit installation. Depending
						on the chosen install method, here we show how to run it using the source installation followed
						by the container version. Below the source install is shown from the directory we created to hold
						all our configuration files:
					</div>
					<div style="height: 100px; text-align: left; font-size: xxx-large;">
						<pre>
							<code data-trim data-noescape>
								# source install.
								#
								$ [PATH_TO]/fluent-bit --config=workshop-fb.yaml
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">OTel integration - Console output envelope pipeline (source)</h3>
					</div>
					<div style="height: 150px; text-align: left; font-size: xx-large;">
						The console output should look something like this. Note each log event has now been put into
						the OTel Envelope format, providing for a <code><b>resource</b></code>, <code><b>scope</b></code>,
						and <code><b>attributes</b></code>. This runs until exiting with CTRL_C:
					</div>
					<div style="height: 100px; text-align: left; font-size: xx-large;">
						<pre>
							<code data-trim data-noescape>
								...
								[2024/08/01 16:26:52] [ info] [input:dummy:dummy.0] initializing
								[2024/08/01 16:26:52] [ info] [input:dummy:dummy.0] storage_strategy='memory' (memory only)
								[2024/08/01 16:26:52] [ info] [output:stdout:stdout.0] worker #0 started
								[2024/08/01 16:26:52] [ info] [sp] stream processor started
								{"date":4294967295.0,"resource":{},"scope":{}}
								{"date":1722522413.113665,"service":"backend","log_entry":"Generating a 200 success code."}
								{"date":4294967294.0}
								{"date":4294967295.0,"resource":{},"scope":{}}
								{"date":1722522414.113558,"service":"backend","log_entry":"Generating a 200 success code."}
								{"date":4294967294.0}
								{"date":4294967295.0,"resource":{},"scope":{}}
								{"date":1722522415.11368,"service":"backend","log_entry":"Generating a 200 success code."}
								...
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">OTel integration - Building envelope pipeline (container)</h3>
					</div>
					<div style="height: 120px; text-align: left; font-size: xx-large;">
						Now we'll build a new container image, naming it with a version tag, as follows using the
						<code><b>Buildfile-fb</b></code> and assuming you are in the same directory:
					</div>
					<div style="height: 200px; text-align: left; font-size: xx-large;">
						<pre>
							<code data-trim data-noescape>
								$ podman build -t workshop-fb:v13 -f Buildfile-fb

								STEP 1/3: FROM cr.fluentbit.io/fluent/fluent-bit:3.2.2
								STEP 2/3: COPY ./workshop-fb.yaml /fluent-bit/etc/workshop-fb.yaml
								--> b4ed3356a842
								STEP 3/3: CMD [ "fluent-bit", "-c", "/fluent-bit/etc/workshop-fb.yaml"]
								COMMIT workshop-fb:v4
								--> bcd69f8a85a0
								Successfully tagged localhost/workshop-fb:v13
								bcd69f8a85a024ac39604013bdf847131ddb06b1827aae91812b57479009e79a
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">OTel integration - Running envelope pipeline (container)</h3>
					</div>
					<div style="height: 50px; text-align: left; font-size: xx-large;">
						Run the initial pipeline using this container command:
					</div>
					<div style="height: 150px; text-align: left; font-size: xxx-large;">
						<pre>
							<code data-trim data-noescape>
								$ podman run --rm --name fbv13 workshop-fb:v13
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">OTel integration - Console output initial pipeline (container)</h3>
					</div>
					<div style="height: 80px; text-align: left; font-size: x-large;">
						The console output should look something like this. Note each log event has now been put into
						the OTel Envelope format, providing for a <code><b>resource</b></code>, <code><b>scope</b></code>,
						and <code><b>attributes</b></code>. This runs until exiting with CTRL_C:
					</div>
					<div style="height: 100px; text-align: left; font-size: xx-large;">
						<pre>
							<code data-trim data-noescape>
								...
								[2024/08/01 16:26:52] [ info] [input:dummy:dummy.0] initializing
								[2024/08/01 16:26:52] [ info] [input:dummy:dummy.0] storage_strategy='memory' (memory only)
								[2024/08/01 16:26:52] [ info] [output:stdout:stdout.0] worker #0 started
								[2024/08/01 16:26:52] [ info] [sp] stream processor started
								{"date":4294967295.0,"resource":{},"scope":{}}
								{"date":1722522413.113665,"service":"backend","log_entry":"Generating a 200 success code."}
								{"date":4294967294.0}
								{"date":4294967295.0,"resource":{},"scope":{}}
								{"date":1722522414.113558,"service":"backend","log_entry":"Generating a 200 success code."}
								{"date":4294967294.0}
								{"date":4294967295.0,"resource":{},"scope":{}}
								{"date":1722522415.11368,"service":"backend","log_entry":"Generating a 200 success code."}
								...
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">OTel integration - Inside the architecture</h3>
					</div>
					<div style="height: 80px; text-align: left; font-size: x-large;">
						The work up to now has been focused on our telemetry pipeline with Fluent Bit. We configured log
						event ingestion, processing to an OTel Envelope, and it's ready to push onwards as shown here in
						this architectural component:
					</div>
					<div style="height: 300px; text-align: right;">
						<img src="images/lab07-2.png" alt="envelope">
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">OTel integration - Pushing to the collector</h3>
					</div>
					<div style="height: 120px; text-align: left; font-size: x-large;">
						When sending telemetry data in the OTel Envelope we are using the OpenTelemetry Protocol (OTLP),
						which is the standard to ingest into a collector. The next step will be for us to configure an
						OTel Collector to receive telemetry data and point our Fluent Bit instances at that collector as
						depicted in this diagram:
					</div>
					<div style="height: 300px;">
						<img src="images/lab07-3.png" alt="collector">
					</div>
				</section>

				<section>
					<div style="height: 100px;">
						<h3 class="r-fit-text">OTel integration - Options installing OTel Collector</h3>
					</div>
					<div style="height: 200px; text-align: left; font-size: xx-large;">
						The rest of this workshop can be done using the path of source installations or container
						installations of the OTel Collector, so please click on the option you want to use for
						the rest of this workshop:
					</div>
					<div style="height: 50px; text-align: left;">
						<ul>
							<li><a href="lab07-podman.html" target="_blank">Installing in a container using Podman (preferred)</a></li>
							<li><a href="lab07-source.html" target="_blank">Installing from the source code</a></li>
						</ul>
					</div>
				</section>
			</div>
		</div>

		<script src="dist/reveal.js"></script>
		<script src="plugin/notes/notes.js"></script>
		<script src="plugin/markdown/markdown.js"></script>
		<script src="plugin/highlight/highlight.js"></script>
		<script>
			// More info about initialization & config:
			// - https://revealjs.com/initialization/
			// - https://revealjs.com/config/
			Reveal.initialize({
				hash: true,

				// Learn about plugins: https://revealjs.com/plugins/
				plugins: [ RevealMarkdown, RevealHighlight, RevealNotes ]
			});
		</script>
		<script src="node_modules/reveal.js-menu/menu.js"></script>
		<script>
			Reveal.initialize({
				plugins: [ RevealMenu ]
			});
		</script>
		<script src="node_modules/reveal.js-menu/menu.js"></script>
		<script>
			Reveal.initialize({
				plugins: [ RevealMenu ]
			});
		</script>
	</body>
</html>
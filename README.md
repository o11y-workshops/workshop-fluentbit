Try the [workshop online](https://o11y-workshops.gitlab.io/workshop-fluentbit): 

[![Cover Slide](cover.png)](https://o11y-workshops.gitlab.io/workshop-fluentbit)

Released versions
-----------------
See the tagged releases for the following versions of the workshop:
 
  - v3.2 - Workshop based on Fluent Bit v3.2, Prometheus v3.0, and OpenTelemetry v0.115.

  - v3.1 - Workshop based on Fluent bit v3.1.